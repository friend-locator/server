var express = require('express');

var app = express();
app.express = express;

app.basedir = __dirname;
app.external_ip = "128.199.237.196";

require('./app/config')(app, function(port, host) {
  app.listen(port, host, function() {
    console.log("Listening on http://" + host + ":" + port)
  });
});
