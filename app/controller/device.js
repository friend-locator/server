module.exports = function(app, cb) {
  var router = app.express.Router();

  router.get("/", function(req, res, next) {
    var deviceId = req.query.id;
    req.user.getDevices(deviceId, function(devices) {
      app.helpers.sendJSON(res, app.helpers.status.OK, devices);
    });
  });

  router.post("/delete", function(req, res, next) {
    var id = req.body.device;
    app.models.Device.findOne({_id: app.models.ObjectId(id), user: req.user._id}, function(err, device) {
      device.sendGcmDelete(function() {
        device.remove();
        app.helpers.sendJSON(res, app.helpers.status.OK, {});
      });
    });
  });

  // create a device
  router.post("/", function(req, res, next) {
    var name = req.body.name;
    var device_id = req.body.device_id;
    var gcm_token = req.body.gcm_token;
    var lat = req.body.lat;
    var lng = req.body.lng;

    req.user.addDevice(name, device_id, gcm_token, lat, lng, function(device) {
      if(device == null) {
        next();
        return;
      }
      req.user.updateLocation(function() {});
      app.helpers.sendJSON(res, app.helpers.status.OK, {
        _id: device._id
      });
    });
  });

  router.all("/:id", function(req, res, next) {
    var id = req.params.id;
    id = app.models.ObjectId(id);
    req.user.getDeviceById(id, function(device) {
      if(device == null) {
        next(new Error("device not found!"));
        return;
      }
      req.device = device;
      next();
    });
  });

  // get device
  router.get("/:id", function(req, res, next) {
    app.helpers.sendJSON(res, app.helpers.status.OK, device);
  });

  cb(router);
};
