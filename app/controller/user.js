module.exports = function(app, cb) {
  var router = app.express.Router();

  router.post("/", function(req, res, next) {
    var authToken = req.body.authToken;

    var gid = "";
    var name = "";
    var photo = "";

    if(authToken == null) {
      gid = req.body.gid;
      name = req.body.name;
      photo = req.body.photo;
      if(gid == null || name == null) {
        next();
        return;
      }
    } else {
      authToken = authToken.split(".");
      if(authToken.length < 2) {
        next();
        return;
      }
      var buf = new Buffer(authToken[1], 'base64');
      var data = JSON.parse(buf.toString());
      if(data.iss != "https://accounts.google.com") {
        next();
        return;
      }

      gid = data.sub;
      name = data.name;
      photo = data.picture;
    }

    photo = photo || "http://"+app.external_ip+"/default_profile_pic.jpg";

    app.models.User.findOne({gid: gid}, function(err, user) {
      if(user == null) {
        user = app.models.User({gid: gid, name: name, photo: photo});
      } else {
        user.name = name;
        user.photo = photo;
      }
      user.save(function(err, user) {
        user.getToken(function(token) {
          user.updateLocation(function(location) {
            if(!location) location = user.location;
            app.helpers.sendJSON(res, app.helpers.status.OK, {token: token, location: location, name: user.name, _id: user._id.toString(), photo: user.photo});
          });
        });
      });
    });
  });

  router.get("/search", function(req, res, next) {
    var query = req.query.query.toLowerCase();
    var start = req.query.start || 0;
    var limit = req.query.limit || 20;
    req.user.search(query, start, limit, function(users) {
      app.helpers.sendJSON(res, app.helpers.status.OK, users);
    });
  });

  router.get("/friends", function(req, res, next) {
    req.user.getFriends(function(friends) {
      app.helpers.sendJSON(res, app.helpers.status.OK, friends);
    });
  });

  router.post("/request", function(req, res, next) {
    var id = req.body.user;
    app.models.User.findOne({_id: app.models.ObjectId(id)}, function(err, user) {
      if(user != null) {
        req.user.addFriend(user, function() {
          app.helpers.sendJSON(res, app.helpers.status.OK, {});
        });
      } else {
        next();
      }
    });
  });

  router.get("/pending", function(req, res, next) {
    req.user.getPendingFriends(function(pendings) {
      app.helpers.sendJSON(res, app.helpers.status.OK, pendings);
    });
  });

  router.get("/request", function(req, res, next) {
    req.user.getRequestedFriends(function(requests) {
      app.helpers.sendJSON(res, app.helpers.status.OK, requests);
    });
  });

  router.post("/remove", function(req, res, next) {
    var id = req.body.user;
    app.models.User.findOne({_id: app.models.ObjectId(id)}, function(err, user) {
      app.models.Device.find({user: user._id}, function(err, devices) {
        req.user.removeFriendById(id, function() {

          var tokens = [];
          for(var i = 0; i < devices.length; i++) {
            tokens.push(devices[i].gcm_token);
          }

          var data = {
            id: req.user._id.toString(),
            type: app.helpers.gcm.FRIEND_REMOVED
          };

          app.helpers.gcm.send(data, tokens, function(sent) {});

          app.helpers.sendJSON(res, app.helpers.status.OK, {});
        });
      });
    });
  });

  router.post("/remove/pending", function(req, res, next) {
    var id = req.body.user;
    app.models.User.findOne({_id: app.models.ObjectId(id)}, function(err, user) {
      app.models.Device.find({user: user._id}, function(err, devices) {
        req.user.removeFriendById(id, function() {

          var tokens = [];
          for(var i = 0; i < devices.length; i++) {
            tokens.push(devices[i].gcm_token);
          }

          var data = {
            id: req.user._id.toString(),
            type: app.helpers.gcm.REQUEST_REMOVED
          };

          app.helpers.gcm.send(data, tokens, function(sent) {});

          app.helpers.sendJSON(res, app.helpers.status.OK, {});
        });
      });
    });
  });

  router.post("/accept", function(req, res, next) {
    var id = req.body.user;
    app.models.User.findOne({_id: app.models.ObjectId(id)}, function(err, user) {
      if(user != null) {
        req.user.acceptFriend(user, function() {
          app.helpers.sendJSON(res, app.helpers.status.OK, {
            _id: user._id,
            name: user.name,
            photo: user.photo,
            location: user.location
          });
        });
      } else {
        next();
      }
    });
  });

  //router.post("/request/location", function(req, res, next) {
  //  var id = req.body.user;
  //  req.user.isFriend(id, function(isFriend) {
  //    if(isFriend) {
  //      
  //    }
  //  });
  //});

  cb(router);
}
