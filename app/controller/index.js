module.exports = function(app, cb) {

  app.use(function(req, res, next) {
    if(req.url != "/user") {
      var token = req.body.token;
      if(req.method.toLowerCase() == "get") {
        token = req.query.token;
      }
      app.models.User.findOne({token: token}, function(err, user) {
        if(user != null) {
          req.user = user;
          next();
          return;
        }
        next(new Error("Invalid Token"));
      });
      return;
    }
    next();
  });

  require("./user")(app, function(router) {

    app.use("/user", router);

    require("./device")(app, function(router) {

      app.use("/device", router);

      app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
      });

      app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        app.helpers.sendJSON(res, app.helpers.status.ACCESS_DENIED, {});
      });

      cb(app);
    });
  });
};
