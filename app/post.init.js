var reqs = [];

function updateUserLocation() {
  if(reqs.length > 0) {
    var req = reqs.shift();
    req.updateLocation(function() {
      updateUserLocation();
    });
  }
}

module.exports = function(app) {
  updateUserLocation();
  var stream = app.models.User.find({}, {lat: 1, lng: 1}).stream();
  stream.on('data', function(doc) {
    reqs.push(doc);
    updateUserLocation();
  }).on('error', function(err) {

  }).on('close', function() {

  });
}
