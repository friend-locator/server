var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function(app) {

  var deviceSchema = new Schema({
    name: {type: String, required: true},
    device_id: {type: String, required: true, unique: true},
    gcm_token: {type: String, required: true, unique: true},
    user: {type: Schema.ObjectId, refs: 'User'},
    lat: {type: Number},
    lng: {type: Number},
    updated_at: { type: Date },
  }, { timestamps: { updatedAt: 'updated_at' }});

  deviceSchema.methods.sendGcmUpdate = function(cb) {
    var device = this;
    app.models.User.findOne({_id: this.user}, function(err, user) {
      user.getFriendIds(function(ids) {
        for(var i = 0; i < ids.length; i++) {
          ids[i] = app.models.ObjectId(ids[i]);
        };
        ids.unshift(0);
        ids[0] = user._id;
        var tokens = [];
        app.models.Device.find({_id: {$ne: device._id}, user: {$in: ids}}, {gcm_token: 1}, function(err, devices) {
          if(devices == null) {
            cb([]);
            return;
          }
          for(var i = 0; i < devices.length; i++) {
            tokens.push(devices[i].gcm_token);
          }

          if(tokens.length <= 0) {
            cb(false);
            return;
          }

          var data = {
            id: device._id.toString(),
            name: device.name,
            user: device.user,
            lat: device.lat,
            lng: device.lng,
            type: app.helpers.gcm.LOCATION_CHANGED
          };

          app.helpers.gcm.send(data, tokens, function(sent) {
            cb(true);
          });
        });
      });
    });
  };

  deviceSchema.methods.sendGcmDelete = function(cb) {
    var device = this;
    app.models.User.findOne({_id: this.user}, function(err, user) {
      user.getFriendIds(function(ids) {
        for(var i = 0; i < ids.length; i++) {
          ids[i] = app.models.ObjectId(ids[i]);
        };
        ids.unshift(0);
        ids[0] = user._id;
        var tokens = [];
        app.models.Device.find({_id: {$ne: device._id}, user: {$in: ids}}, {gcm_token: 1}, function(err, devices) {
          if(devices == null) {
            cb([]);
            return;
          }
          for(var i = 0; i < devices.length; i++) {
            tokens.push(devices[i].gcm_token);
          }

          if(tokens.length <= 0) {
            cb(false);
            return;
          }

          var data = {
            id: device._id.toString(),
            type: app.helpers.gcm.DEVICE_REMOVED
          };

          app.helpers.gcm.send(data, tokens, function(sent) {
            cb(true);
          });
        });
      });
    });
  };

  var Device = mongoose.model('Device', deviceSchema);

  return Device;
}
