var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function(app) {

  var userSchema = new Schema({
    gid: { type: Number, required: true, unique: true },
    name: { type: String, required: true },
    photo: { type: String, required: true },
    token: { type: String, unique: true },
    lat: { type: Number, default: 0 },
    lng: { type: Number, default: 0 },
    location: { type: String, default: "" }
  });

  // INSTANCE METHODS

  userSchema.methods.getToken = function(cb) {
    var time = Date.now();
    var token = app.helpers.crypto.sha1(this.gid.toString() + this.name + time.toString());
    this.token = token;
    var that = this;
    app.models.User.findOne({token: token}, function(err, user) {
      if(user != null) {
        that.getToken(cb);
      } else {
        that.token = token;
        that.save(function(err, user) {
          that.notifyTokenChanged();
          cb(token);
        });
      }
    });
  };

  userSchema.methods.addFriend = function(user, cb) {
    var connection = app.models.Connection({
      user1: this._id,
      user2: user._id
    });

    var that = this;

    connection.save(function(err, connection) {
      app.models.Device.find({user: user._id}, {gcm_token : 1}, function(err, devices) {
        var tokens = [];
        for(var i = 0; i < devices.length; i++) {
          tokens.push(devices[i].gcm_token);
        }
        if(tokens.length <= 0) {
          cb();
          return;
        }
        app.helpers.gcm.send({
          id: that._id,
          name: that.name,
          photo: that.photo,
          location: that.location,
          type: app.helpers.gcm.FRIEND_REQUESTED
        }, tokens, function(err, res, body) {
        });
      });
      cb();
    });
  };

  userSchema.methods.removeFriend = function(user, cb) {
    this.removeFriendById(user._id, cb);
  };

  userSchema.methods.removeFriendById = function(id, cb) {
    id = app.models.ObjectId(id.toString());
    app.models.Connection.findOneAndRemove({
      $or: [
        {$and: [{user1: this._id}, {user2: id}]},
        {$and: [{user1: id}, {user2: this._id}]}
      ]}, function(err, connection) {
        cb();
    });
  };

  userSchema.methods.getFriendIds = function(cb) {
    var user = this;
    app.models.Connection.find({$or: [{user1: this._id}, {user2: this._id}], accepted: true}, {user1: 1, user2: 1}, function(err, connections) {
      var users = [];
      for(var i = 0; i < connections.length; i++) {
        var id = (user._id.toString() == connections[i].user1.toString()) ? connections[i].user2 : connections[i].user1;
        users.push(id.toString());
      }
      cb(users);
    });
  };

  userSchema.methods.getFriends = function(cb) {
    this.getFriendIds(function(ids) {
      for(var i = 0; i < ids.length; i++) {
        ids[i] = app.models.ObjectId(ids[i].toString());
      }
      app.models.User.find({_id: {$in: ids}}, {name: 1, photo: 1, location: 1}, function(err, users) {
        if(!users) users = [];
        cb(users);
      });
    });
  };

  userSchema.methods.getPendingFriendIdsWithDate = function(cb) {
    app.models.Connection.find({user1: this._id, accepted: false}, {user2: 1, created_at: 1, location: 1}, function(err, connections) {
      var users = {};
      for(var i = 0; i < connections.length; i++) {
        var id = connections[i].user2;
        var date = connections[i].created_at;
        users[id] = date;
      }
      cb(users);
    });
  };

  userSchema.methods.getPendingFriendIds = function(cb) {
    app.models.Connection.find({user1: this._id, accepted: false}, {user2: 1}, function(err, connections) {
      var users = [];
      for(var i = 0; i < connections.length; i++) {
        var id = connections[i].user2;
        users.push(id.toString());
      }
      cb(users);
    });
  };

  userSchema.methods.getPendingFriends = function(cb) {
    this.getPendingFriendIdsWithDate(function(us) {
      var ids = [];
      for(var id in us) {
        ids.push(app.models.ObjectId(id))
      }
      app.models.User.find({_id: {$in: ids}}, {name: 1, photo: 1, location: 1}, function(err, users) {
        if(!users) {
          cb([]);
          return;
        }

        var u = [];
        for(var i = 0; i < users.length; i++) {
          u.push({
            _id: users[i]._id,
            name: users[i].name,
            photo: users[i].photo,
            location: users[i].location,
            created_at: us[users[i]._id]
          });
        }
        cb(u);
      });
    });
  };

  userSchema.methods.getRequestedFriendIdsWithDate = function(cb) {
    app.models.Connection.find({user2: this._id, accepted: false}, {user1: 1, created_at: 1}, function(err, connections) {
      var users = {};
      for(var i = 0; i < connections.length; i++) {
        var id = connections[i].user1;
        var date = connections[i].created_at;
        users[id] = date;
      }
      cb(users);
    });
  };

  userSchema.methods.getRequestedFriendIds = function(cb) {
    app.models.Connection.find({$or: [{user2: this._id}], accepted: false}, {user1: 1}, function(err, connections) {
      var users = [];
      for(var i = 0; i < connections.length; i++) {
        var id = connections[i].user1;
        users.push(id.toString());
      }
      cb(users);
    });
  };

  userSchema.methods.getRequestedFriends = function(cb) {
    this.getRequestedFriendIdsWithDate(function(us) {
      var ids = [];
      for(var id in us) {
        ids.push(app.models.ObjectId(id))
      }
      app.models.User.find({_id: {$in: ids}}, {name: 1, photo: 1, location: 1}, function(err, users) {
        if(!users) {
          cb([]);
          return;
        }

        var u = [];
        for(var i = 0; i < users.length; i++) {
          u.push({
            _id: users[i]._id,
            name: users[i].name,
            photo: users[i].photo,
            location: users[i].location,
            created_at: us[users[i]._id]
          });
        }
        cb(u);
      });
    });
  };

  userSchema.methods.acceptFriend = function(user, cb) {
    var that = this;
    app.models.Connection.findOneAndUpdate({user1: user._id, user2: this._id}, {$set: {accepted: true}}, {new: true}, function(err, connection) {
      if(connection != null) {
        app.models.Device.find({user: user._id}, {gcm_token : 1}, function(err, devices) {
          var tokens = [];
          for(var i = 0; i < devices.length; i++) {
            tokens.push(devices[i].gcm_token);
          }
          app.helpers.gcm.send({
            id: that._id,
            name: that.name,
            photo: that.photo,
            location: that.location,
            type: app.helpers.gcm.REQUEST_ACCEPTED
          }, tokens, function() {
          });
        });
        cb();
      } else {
        cb();
      }
    });
  };

  userSchema.methods.isFriend = function(userId, cb) {
    userId = app.models.ObjectId(userId.toString());
    app.models.Connection.findOne({$or: [{$and: [{user1: userId}, {user2: this._id}]}, {$and: [{user2: userId}, {user1: this._id}]}]}, {}, function(err, connection) {
      cb(connection != null);
    });
  };

  userSchema.methods.addDevice = function(name, device_id, gcm_token, lat, lng, cb) {
    var user = this;
    app.models.Device.findOne({device_id: device_id}, function(err, device) {
      if(device == null) {
        lat = (lat == null) ? 0 : lat;
        lng = (lng == null) ? 0 : lng;
        device = app.models.Device({name: name, device_id: device_id, gcm_token: gcm_token, lat: lat, lng: lng, user: user._id});
      } else {
        if(lat != null && lng != null) {
          device.lat = lat;
          device.lng = lng;
          app.helpers.geocoder.findIfNecessary(lat, lng, user.lat, user.lng, function() {});
        }
        device.user = user._id;
        if(gcm_token != null) device.gcm_token = gcm_token;
        if(name != null) device.name = name;
      }

      if(device.isNew) {
        app.models.Device.findOneAndRemove({gcm_token: gcm_token}, function() {
          device.save(function(err, device) {
            device.sendGcmUpdate(function() {});
            cb({_id: device._id});
          });
        });
      } else {
        device.save(function(err, device) {
          device.sendGcmUpdate(function() {});
          cb({_id: device._id});
        });
      }


    });
  };

  userSchema.methods.getDevices = function(deviceId, cb) {
    var user = this;
    deviceId = app.models.ObjectId(deviceId);
    this.getFriendIds(function(ids) {
      for(var i = 0; i < ids.length; i++) {
        ids[i] = app.models.ObjectId(ids[i]);
      }
      ids.unshift(0);
      ids[0] = user._id;
      app.models.Device.find({user: {$in: ids}, _id: {$ne: deviceId} }, {name: 1, updated_at: 1, lat: 1, lng: 1, user: 1}, function(err, devices) {
        cb(devices);
      });
    });
  };

  userSchema.methods.getDeviceById = function(id, cb) {
    var user = this;
    this.getFriendIds(function(ids) {
      id = app.models.ObjectId(id.toString());
      for(var i = 0; i < ids.length; i++) {
        ids[i] = app.models.ObjectId(ids[i]);
      }
      ids.unshift(0);
      ids[0] = user._id;
      app.models.Device.findOne({user: {$in: ids}, _id: id}, {name: 1, updated_at: 1, lat: 1, lng: 1, user: 1}, function(err, device) {
        cb(device);
      });
    });
  };

  userSchema.methods.removeDeviceById = function(id, cb) {
    id = app.models.ObjectId(id.toString());
    app.models.Device.findOneAndRemove({user: this._id, _id: id}, function(err, device) {
      cb();
    });
  };

  userSchema.methods.search = function(name, start, limit, cb) {

    var re = new RegExp(name, "i");
    if(name.trim() == "") {
      cb([]);
      return;
    }
    var user = this;
    app.models.User.find({name: re}, {name: 1, photo: 1, location: 1}, function(err, users) {
      if(users.length > 0) {
        user.getFriendIds(function(friendIds) {
          user.getRequestedFriendIds(function(requestedIds) {
            user.getPendingFriendIds(function(pendingIds) {
              var pending = requestedIds.concat(pendingIds);
              for(var i = 0; i < users.length; i++) {
                var u = users[i].toObject();
                u.isFriend = (friendIds.indexOf(users[i]._id.toString()) != -1);
                u.isPending = (pending.indexOf(users[i]._id.toString()) != -1);
                users[i] = u;
              }
              cb(users);
            });
          });
        });
      } else {
        cb(users);
      }
    }).sort({name: 1}).skip(Number(start)).limit(Number(limit));
  };

  userSchema.methods.updateLocation = function(cb) {
    var user = this;
    app.models.Device.findOne({user: this._id}, function(err, device) {
      if(device == null) {
        cb(false);
        return;
      }
      var lat = device.lat;
      var lng = device.lng;
      if(lat != null && lng != null) {
        app.helpers.geocoder.findIfNecessary(lat, lng, user.lat, user.lng, function(loc) {
          if(loc != null) {
            user.location = loc;
            user.notifyDevices();
          }
          user.lat = lat;
          user.lng = lng;
          user.save(function(err, user) {
            cb(loc);
          });
        });
      } else {
        cb(false);
      }
    }).sort({updated_at: -1});
  };

  userSchema.methods.notifyDevices = function() {
    var user = this;
    app.models.Connection.find({$or: [{user1: this._id}, {user2: this._id}]}, {user1: 1, user2: 1}, function(err, connections) {
      var users = [];
      for(var i = 0; i < connections.length; i++) {
        var id = (user._id.toString() == connections[i].user1.toString()) ? connections[i].user2 : connections[i].user1;
        users.push(id);
      }
      app.models.Device.find({user: {$in: users}}, {gcm_token: 1}, function(err, devices) {
        var tokens = [];
        for(var i = 0; i < devices.length; i++) {
          tokens.push(devices[i].gcm_token);
        }
        var data = {
          type: app.helpers.gcm.USER_CHANGED,
          id: user._id.toString(),
          name: user.name,
          location: user.location,
          photo: user.photo
        };
        app.helpers.gcm.send(data, tokens, function() {});
      });
    });
  };

  userSchema.methods.notifyTokenChanged = function() {
    var user = this;
    app.models.Device.find({user: user._id}, {gcm_token: 1}, function(err, devices) {
      var tokens = [];
      for(var i = 0; i < devices.length; i++) {
        tokens.push(devices[i].gcm_token);
      }
      var data = {
        type: app.helpers.gcm.TOKEN_CHANGED,
        token: user.token
      };
      app.helpers.gcm.send(data, tokens, function() {});
    });
  };

  var User = mongoose.model('User', userSchema);

  return User;
}
