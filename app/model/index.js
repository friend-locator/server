var mongoose = require("mongoose");

module.exports = function(app, cb) {

  var config = {
    port: 27017,
    host: "127.0.0.1",
    dbname: "friend-locator",
    user: "",
    pass: ""
  };

  mongoose.connect("mongodb://"+config.host+":"+config.port+"/"+config.dbname);

  app.models = {};

  app.models.ObjectId = mongoose.Types.ObjectId;

  app.models.Connection = require("./connection")(app);
  app.models.User = require("./user")(app);
  app.models.Device = require("./device")(app);

  cb(app);
}
