var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function(app) {

  var connectionSchema = new Schema({
    user1: {type: Schema.ObjectId, refs: 'User'},
    user2: {type: Schema.ObjectId, refs: 'User'},
    accepted: {type: Boolean, default: false},
    created_at: { type: Date },
  }, { timestamps: { createdAt: 'created_at' }});

  connectionSchema.pre('save', function(next, done) {
    
    if(!this.isNew){
      next();
      return;
    }

    app.models.Connection.findOne({
      $or: [
        {$and: [{user1: this.user1}, {user2: this.user2}]},
        {$and: [{user1: this.user2}, {user2: this.user1}]}
      ]}, function(err, connection) {
        if(connection != null) {
          done(new Error("Connection already exists"));
          return;
        }
        next();
    });
  });

  var Connection = mongoose.model('Connection', connectionSchema);

  return Connection;
}
