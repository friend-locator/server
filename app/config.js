var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var configs = {
  port: 3000,
  host: 'localhost'
}

module.exports = function(app, cb) {

  app.use(logger('common'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(app.express.static(path.join(app.basedir,'public')));

  require('./helpers')(app, function(app) {
    require('./model')(app, function(app) {
      require('./controller')(app, function(app) {
        require('./post.init.js')(app);
        cb(configs.port, configs.host);
      });
    });
  });
}
