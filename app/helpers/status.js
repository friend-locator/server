module.exports = function(app, cb) {
  return {
    OK: 101,


    ACCESS_DENIED: 102,


    BAD_TOKEN: 103,
    USER_EXISTS: 104
  };
};
