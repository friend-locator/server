module.exports = function(app, cb) {
  app.helpers = {};

  app.helpers.crypto = require('./crypto')(app);
  app.helpers.gcm = require('./gcm')(app);
  app.helpers.status = require('./status')(app);
  app.helpers.geocoder = require('./geocoder')(app);

  app.helpers.sendJSON = function(res, status, body) {
    res.json({
      status: status,
      response: body
    });
  };

  cb(app);
};
