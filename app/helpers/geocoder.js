var request = require('request');

var API_KEY = "AIzaSyDsgEa4ymk2ISltqck7p76TKFSrkh__1OM";
var GEOCODER_URL = "https://maps.googleapis.com/maps/api/geocode/json?key="+API_KEY;

module.exports = function(app) {

  function distance(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
  }

  function deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  return {
    findIfNecessary: function(lat1, lng1, lat2, lng2, cb) {
      if(lat1 == null || lng1 == null) {
        cb(null);
        return;
      }
      if(lat2 == null || lng2 == null) {
        this.find(lat1, lng1, cb);
      } else {
        d = distance(lat1, lng1, lat2, lng2);
        if(d > 1) {
          this.find(lat1, lng1, cb);
        } else {
          cb(null);
        }
      }
    },
    find: function(lat, lng, cb) {
      var url = GEOCODER_URL + "&latlng=" + lat + "," + lng + "&components=country";
      var options = {
        url: url
      }
      request(options, function(err, res, body) {
        var body = JSON.parse(body);
        if(body.status == "OK") {
          // 3 5 6
          var address = body.results[0].address_components;
          var result = "";
          for(var i = 0; i < address.length; i++) {
            if(address[i].types.indexOf("locality") != -1 || address[i].types.indexOf("country") != -1) {
              result += address[i].long_name + ", ";
            }
          }
          result = result.slice(0, -", ".length);
          cb(result);
        } else {
          cb(null);
        }
      });
    }
  };
}
