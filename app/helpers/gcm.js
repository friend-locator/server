var request = require('request');

var API_KEY = "AIzaSyDsgEa4ymk2ISltqck7p76TKFSrkh__1OM";

var GCM_URL = "https://gcm-http.googleapis.com/gcm/send";

module.exports = function(app) {

  var sendGCM = function(data, tokens) {
    var options = {
      url: GCM_URL,
      headers: {
        'Authorization': "key="+API_KEY,
        'Content-Type': 'application/json'
      },
      method: 'POST',
      json: {
        registration_ids: tokens,
        data: data
      }
    };

    request(options, function(err, res, body) {});
  }

  return {
    send: function(data, tokens, cb) {
      var n = Math.ceil(tokens.length / 1000);
      for(var i = 0; i < n; i++) {
        var end = (tokens.length > (i+1)*1000) ? (i+1)*1000 : tokens.length;
        var t = tokens.slice(i*1000, end);
        sendGCM(data, t);
      }
      cb(true);
    },

    LOCATION_CHANGED: 0,
    FRIEND_REMOVED: 1,
    REQUEST_ACCEPTED: 2,
    FRIEND_CHANGED: 3,
    FRIEND_REQUESTED: 4,
    REQUEST_REMOVED: 5,
    DEVICE_REMOVED: 6,
    USER_CHANGED: 7,
    TOKEN_CHANGED: 8,
    REQUEST_FRIEND_LOCATION: 9
  };
};
