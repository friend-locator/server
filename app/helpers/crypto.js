var crypto = require("crypto");

module.exports = function(app, cb) {
  return {
    sha1: function(input){
        return crypto.createHash('sha1').update(input).digest('hex');
    },
    sha256: function(input){
        return crypto.createHash('sha256').update(input).digest('hex');
    }
  };
};
