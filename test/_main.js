global.chai = require('chai');
global.expect = chai.expect;
global.assert = chai.assert;
global.request = require('request');
global.app = {};


require("../app/helpers")(global.app, function() {
  require("../app/model")(global.app, function() {
    var app = global.app;
    before(function(done) {
      app.models.User.remove({}, function(err) {
        app.models.Connection.remove({}, function(err) {
          app.models.Device.remove({}, function(err) {
            done();
          });
        });
      });
    });
  });
});
