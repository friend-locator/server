var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var request = require('request');
var app = {};

var geocoder = require("../app/helpers/geocoder")(app);

describe("Geocoder Testing", function() {
  it("Reverse Coding New York", function(done) {
    geocoder.find(40.711652, -74.006814, function(res) {
      done();
    });
  });
  it("Reverse Coding Galle", function(done) {
    geocoder.find(6.043484, 80.209295, function(res) {
      done();
    });
  });
  it("Reverse Coding Muscat", function(done) {
    geocoder.find(23.589006, 58.406347, function(res) {
      done();
    });
  });
});
