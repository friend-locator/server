var chai = global.chai;
var expect = global.expect;
var assert = global.assert;
var app = global.app;

describe("User Model Testing", function() {
  var user1;
  var user2;

  var deviceId;
  var deviceToken;

  it("initialize user1", function() {
    user1 = app.models.User({
      gid: 23623423415234,
      name: "Tharindu Hasthika",
      photo: "http://google.com",
      token: "h3g24fwef23r23r3gwefw32"
    });
    expect(user1).to.not.equal(null);
  });

  it("initialize user2", function() {
    user2 = app.models.User({
      gid: 3456345234234234,
      name: "John Snow",
      photo: "http://google.com/asfasd",
      token: "h3g24fwef23r23r3g23sdf322"
    });
    expect(user2).to.not.equal(null);
  });

  it("create user1", function(done) {
    user1.save(function(err, user1) {
      expect(user1.name).to.equal("Tharindu Hasthika");
      done();
    });
  });

  it("create user2", function(done) {
    user2.save(function(err, user2) {
      expect(user2.name).to.equal("John Snow");
      done();
    });
  });

  it("create token", function(done) {
    user1.getToken(function(token) {
      expect(token).to.not.equal(null);
      done();
    });
  });

  it("add device", function(done) {
    user1.addDevice("HTC M8", "rsgwsvsvse23423412hqda", function(device) {
      deviceId = device._id;
      deviceToken = device.gcm_token;
      user2.addDevice("HTC M9", "rsgwsvsvse23443412hqda", function(device) {
        done();
      });
    });
  });

  it("get device by token", function(done) {
    user1.getDeviceByToken(deviceToken, function(device) {
      expect(device).to.not.equal(null);
      done();
    });
  });

  it("add friend", function(done) {
    user1.addFriend(user2, function() {
      done();
    });
  });

  it("get pending friends", function(done) {
    user1.getPendingFriends(function(users) {
      done();
    });
  });

  it("accept friend", function(done) {
    user2.acceptFriend(user1, function() {
      done();
    });
  });

  it("get friends", function(done) {
    user1.getFriends(function(users) {
      done();
    });
  });

  it("get devices", function(done) {
    user1.getDevices(function(devices) {
      done();
    });
  });

  it("get device by id", function(done) {
    user1.getDeviceById(deviceId, function(device) {
      expect(device).to.not.equal(null);
      done();
    });
  });

  it("is friends", function(done) {
    user1.isFriend(user2._id, function(isFriend) {
      expect(isFriend).to.equal(true);
      done();
    });
  });

  it("remove friend", function(done) {
    user1.removeFriend(user2, function() {
      done();
    });
  });

});

describe("Testing User Static Methods", function() {
  it("search method", function(done) {
    app.models.User.search("Th", function(users) {
      expect(users.length).to.equal(1);
      done();
    });
  });
});
