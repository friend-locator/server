var chai = global.chai;
var expect = global.expect;
var assert = global.assert;
var app = global.app;
var request = global.request;

var fs = require("fs");

function getUrl(path) {
  return "http://localhost:3000" + path;
}

describe("User Controller Testing", function() {
  var token1;
  var token2;

  var user_id1;
  var user_id2;

  var device_id1;
  var device_id2;

  it("create or update a user", function(done) {
    var authToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjYyMjAyZGUyY2ExOGE0ZGNiMGFhN2EwNTY1ZTkzZjI1Nzg5YjNlMmQifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhdWQiOiI2MDU5OTc5MzUyOTgtaGY4aHN0cGdoMjY4MmRqYzQ4Y3FycWl1dG43NW0wNHQuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTQwNjc0NTY2MDA4MTc4NDc3MDkiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiNjA1OTk3OTM1Mjk4LWNwc2ljcjFjYTlzZmh1YXVoNWh2YjlzYmNiamVlZTUzLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiZW1haWwiOiJ0aGFyaW5kdWJhdGhpZ2FtYUBnbWFpbC5jb20iLCJpYXQiOjE0NTYxMjkyODcsImV4cCI6MTQ1NjEzMjg4NywibmFtZSI6IlRoYXJpbmR1IEhhc3RoaWthIiwicGljdHVyZSI6Imh0dHBzOi8vbGg0Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tRFM2MnFTZjF2REkvQUFBQUFBQUFBQUkvQUFBQUFBQUFEWDAvZ0xwdUlSakV1WE0vczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IlRoYXJpbmR1IiwiZmFtaWx5X25hbWUiOiJIYXN0aGlrYSIsImxvY2FsZSI6ImVuIn0.eToKcYH12dwoVhGWTjE2w4loEeOlHoFSJ7THdGD3IqBmYCJin0LECU6prM0198MvltjFqzYzYtsOMAiKehNqHJakNvMs160uaN5KOvkgrfVsOwUF-dsU3J2OADI7wQxOOMD8ZuFIu2J2Z3ymHEy6zCev5Obv2lTQEIiAeErC_WpBkh1yu1EtyWUkjHILfQWfA5EiOKDhyuomXJRDsO0TO_l87uBh6smTx_ZfWE419RDrfuwKtkw3TGC-DGzFAt5Y5nSRrDb1qQoag5lTzXFK2AVHYJGRAXy-tWOr8JY2blAWDpw3BQGpk-lQFtXHODmvBSOGwtKclPgznXgwrXhT4A";

    request({
      url: getUrl("/user"),
      method: "post",
      json: {
        authToken: authToken
      }
    }, function(err, res, body) {
      token1 = body.response.token;
      done();
    });
  });

  it("create or update another user", function(done) {
    var authToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjYyMjAyZGUyY2ExOGE0ZGNiMGFhN2EwNTY1ZTkzZjI1Nzg5YjNlMmQifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhdWQiOiI2MDU5OTc5MzUyOTgtaGY4aHN0cGdoMjY4MmRqYzQ4Y3FycWl1dG43NW0wNHQuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDM2Nzk0MTA1MTk3NDM1OTQ3NjAiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiNjA1OTk3OTM1Mjk4LWNwc2ljcjFjYTlzZmh1YXVoNWh2YjlzYmNiamVlZTUzLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiZW1haWwiOiJhbWFuZGFtcnNuZ2gzMEBnbWFpbC5jb20iLCJpYXQiOjE0NTYxNTIwNTAsImV4cCI6MTQ1NjE1NTY1MCwibmFtZSI6IkFtYW5keiBNYXJlIiwicGljdHVyZSI6Imh0dHBzOi8vbGg2Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tVEpER05Ca044SkUvQUFBQUFBQUFBQUkvQUFBQUFBQUFBMlUvakwxOHdsVmF6Z00vczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IkFtYW5keiIsImZhbWlseV9uYW1lIjoiTWFyZSIsImxvY2FsZSI6ImVuLUdCIn0.HR8gv75oAMWlLNBBIXHhgiGlbRm22XsqjTXxa5BfCrkZDcDqGP_8r_0NJ5dMPp4k1jtjotlCmQGZnG5-9GjjmiDoPH-6H0MJ3jzjvjCw4D5xRBRV4gIQjlZ64PJH-6FAQmcrpRmHlNlCiBl2VHz8WsP9IW5A53kfi3HYjVw0RhFJyf9ky6vh7MREsaZJaJ0O2auT7hmQ8_oXBLJVro4HmgxgEMlrTnyHqDGjTS6zml31UoRYwYXsAlbRKZTCzRVTF_Ek_mcz7fSBPGIjLT5MUY516LfFzws-OzQf1PbQo_Cw4AoBNu8aakvGTSHYJn5u0QgDT-StYgNVNN6Y_FPPag";

    request({
      url: getUrl("/user"),
      method: "post",
      json: {
        authToken: authToken
      }
    }, function(err, res, body) {
      token2 = body.response.token;
      done();
    });
  });

  it("search for a user", function(done) {
    request({
      url: getUrl("/user/search"),
      method: "get",
      qs: {
        token: token1,
        query: "Aman"
      }
    }, function(err, res, body) {
      body = JSON.parse(body);
      if(body.status == 102) {
        done();
        return;
      }
      user_id2 = body.response[0]._id;
      done();
    });
  });

  it("search for another user", function(done) {
    request({
      url: getUrl("/user/search"),
      method: "get",
      qs: {
        token: token1,
        query: "Thar"
      }
    }, function(err, res, body) {
      body = JSON.parse(body);
      if(body.status == 102) {
        done();
        return;
      }
      user_id1 = body.response[0]._id;
      done();
    });
  });

  it("send friend request", function(done) {
    request({
      url: getUrl("/user/request"),
      method: "post",
      json: {
        token: token1,
        user: user_id2
      }
    }, function(err, res, body) {
      done();
    });
  });

  it("get pending friends", function(done) {
    request({
      url: getUrl("/user/pending"),
      method: "get",
      qs: {
        token: token1
      }
    }, function(err, res, body) {
      body = JSON.parse(body);
      done();
    });
  });

  it("get request friends", function(done) {
    request({
      url: getUrl("/user/request"),
      method: "get",
      qs: {
        token: token2
      }
    }, function(err, res, body) {
      body = JSON.parse(body);
      done();
    });
  });

  it("accept friend", function(done) {
    request({
      url: getUrl("/user/accept"),
      method: "post",
      json: {
        token: token2,
        user: user_id1
      }
    }, function(err, res, body) {
      done();
    });
  });

  it("get user1 friends", function(done) {
    request({
      url: getUrl("/user/friends"),
      method: "get",
      qs: {
        token: token1
      }
    }, function(err, res, body) {
      body = JSON.parse(body);
      done();
    });
  });

  it("get user2 friends", function(done) {
    request({
      url: getUrl("/user/friends"),
      method: "get",
      qs: {
        token: token2
      }
    }, function(err, res, body) {
      body = JSON.parse(body);
      done();
    });
  });

  it("add device to user1", function(done) {
    request({
      url: getUrl("/device"),
      method: "post",
      json: {
        token: token1,
        name: "HTC ONE M8",
        gcm_token: "235wefw4f23fqef1eqwdw",
        lat: 32.4235234,
        lng: 24.4233241
      }
    }, function(err, res, body) {
      device_id1 = body.response._id;
      done();
    });
  });

  it("add device to user2", function(done) {
    request({
      url: getUrl("/device"),
      method: "post",
      json: {
        token: token2,
        name: "HTC ONE M9",
        gcm_token: "235we32423fw4f23fqef1eqwdw",
        lat: 32.4235234,
        lng: 24.4233241
      }
    }, function(err, res, body) {
      device_id2 = body.response._id;
      done();
    });
  });

  it("get devices", function(done) {
    request({
      url: getUrl("/device"),
      method: "get",
      qs: {
        token: token1
      }
    }, function(err, res, body) {
      body = JSON.parse(body);
      done();
    });
  });

});
