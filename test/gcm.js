var request = require('request');

var API_KEY = "AIzaSyDsgEa4ymk2ISltqck7p76TKFSrkh__1OM";

var GCM_URL = "https://gcm-http.googleapis.com/gcm/send";

var LOCATION_CHANGED = 0
var FRIEND_REMOVED = 1
var REQUEST_ACCEPTED = 2;
var FRIEND_CHANGED = 3;
var FRIEND_REQUESTED = 4;
var REQUEST_REMOVED = 5;
var DEVICE_REMOVED = 6;
var USER_CHANGED = 7;
var TOKEN_CHANGED = 8;

var sendGCM = function(data, tokens, cb) {
  var options = {
    url: GCM_URL,
    headers: {
      'Authorization': "key="+API_KEY,
      'Content-Type': 'application/json'
    },
    method: 'POST',
    json: {
      registration_ids: tokens,
      data: data
    }
  };

  request(options, cb);
}

var send = function(data, tokens, cb) {
  var n = Math.ceil(tokens.length / 1000);
  var A = [];
  var B = [];
  var C = [];
  for(var i = 0; i < n; i++) {
    var end = (tokens.length > (i+1)*1000) ? (i+1)*1000 : tokens.length;
    var t = tokens.slice(i*1000, end);
    (function(i, t) {
      sendGCM(data, t, function(a, b, c) {
        A.push(a);
        B.push(b);
        C.push(c);
        if(i == n-1) {
          cb(A, B, C);
        }
      });
    })(i, t);
  }
};

describe("GCM Testing", function() {
  it("GCM SENT", function(done) {
    var tokens = ["d6jeyWxGiM0:APA91bFxtp6KD2N2amEU1R3SSGZj_-Oka8QaG_VyNnxNZOQhVJZVAYXPdIGsa7-z_aRBUYCe3tiphtiuJW5iCDqZLnj1GpDlydscnWvikb_n__ZCCwlQztyofmcRtBe01B8r2ukwSUq-"];
    send({type: TOKEN_CHANGED}, tokens, function(err, res, body) {
      console.log(body);
      done();
    });
  });
});
